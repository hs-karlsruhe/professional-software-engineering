Merge requests
==============

.. contents:: Table of contents
    :local:

For more details have a look at the official GitLab documentation `Merge requests <https://docs.gitlab.com/ee/user/project/merge_requests/>`_.

Create a merge request
----------------------

An overview of all merge requests can be seen in the "Merge Requests" section. A new merge request can be created by clicking "New merge request".

   .. figure:: images/show-merge-requests.png

Select source and target branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Select a source branch and the target branch to merge into.

   .. figure:: images/new-merge-request-compare.png

Submit merge request
~~~~~~~~~~~~~~~~~~~~

If there is only one commit in the source branch which would be merged into the target branch, the title is automatically set to that commit message. Otherwise, the name of the branch is used.

The merge request title should always represent the changes made in the merge request. So if the autocompleted title is not sufficient for this purpose, it should be adjusted.

When submitting a merge request, information of the purpose a description what changed should be added in the description field.

It is highly recommended to delete the source branch after the merge request is merged by GitLab so that the project keeps clean and the amount of branches is set to a minimum.

   .. figure:: images/new-merge-request-create.png

   .. figure:: images/new-merge-request-submit.png

After submitting the merge request, an overview of the request is shown.

Review a merge request
----------------------

For more details have a look at the official GitLab documentation `Reviewing and managing merge requests <https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html>`_.

Reviews can be started after opening the changes list of a merge request.

   .. figure:: images/new-merge-request-changes.png

All changed files are listed with their diff.

When hovering on the left over the line numbers, the button "Add a comment to this line" appears. A click on this button opens a textbox to put a comment into.

After a message is written, the review can be started and a new thread is created. More threads can be added by repeating the steps described above. For every thread, more comments can be written.

There are different types of comments. On the one hand, the reviewer can request for more background information or on the other hand, he can request or propose a code change.

   .. figure:: images/new-merge-request-start-review.png

When the review is finished, it can be submitted.

   .. figure:: images/new-merge-request-submit-review.png

After submitting the review, the developer of the merge request receives a notification. He can now work on the comments. When a code change is requested, the change is committed in the source branch of the merge request and pushed to GitLab. After finishing the work on a comment, the thread can be marked as resolved.

   .. figure:: images/new-merge-request-comment-review.png

When all threads are resolved (shown at the right to the merge request navigation tabs), the merge request is ready to merge from the perspective of the submitter.

   .. figure:: images/new-merge-request-threads-resolved.png


Merge merge request
-------------------

A merge request can be merged by clicking the "Merge" button.

   .. figure:: images/new-merge-request-merge.png

In the background, GitLab executes the command ``git merge testing`` (as our branch is named ``testing`` here) and merges the changes directly into the target branch (here ``master``). Any local history is now out of date, so the changes must be fetched from the remote.

Besides the commit "Set name to real name", a merge commit is created by GitLab with the name "Merge branch 'testing' into 'master'".

   .. figure:: images/new-merge-request-commit-history.png

In local Git clones, the latest commits are not included in the target branch. It can be verified with ``git log``. After pulling the changes from the remote, the local branch is also updated.
