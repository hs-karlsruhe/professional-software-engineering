Git
===

.. toctree::
    :maxdepth: 3
    :caption: Content

    installation
    projects
    cheatsheet
    committing
    forking
