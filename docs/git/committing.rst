Committing
==========

.. contents:: Table of contents
    :local:

Golden rules of writing commit messages
---------------------------------------

According to `<https://www.freecodecamp.org/news/writing-good-commit-messages-a-practical-guide/>`__.

#. Separate the subject from the body with a blank line
#. Your commit message should not contain any whitespace errors
#. Remove unnecessary punctuation marks
#. Do not end the subject line with a period
#. Capitalize the subject line and each paragraph
#. Use the imperative mood in the subject line
#. Use the body to explain what changes you have made and why you made
   them.
#. Do not assume the reviewer understands what the original problem
   was, ensure you add it.
#. Do not think your code is self-explanatory
#. Follow the commit convention defined by your team

