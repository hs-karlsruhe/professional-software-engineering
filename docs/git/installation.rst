Installation
============

.. contents:: Table of contents
    :local:

Windows
-------

Download Git from https://git-scm.com/downloads and run the executable. Use the proposed settings during installation:

Uncheck checkbox "Only show new options":

.. figure:: images/setup-1.png

.. figure:: images/setup-2.png

Choose editor Nano:

.. figure:: images/setup-3.png

.. figure:: images/setup-4.png

.. figure:: images/setup-5.png

.. figure:: images/setup-6.png

Choose "Checkout as-is, commit Unix-style line endings":

.. figure:: images/setup-7.png

.. figure:: images/setup-8.png

Choose radio button "Rebase":

.. figure:: images/setup-9.png

.. figure:: images/setup-10.png

.. figure:: images/setup-11.png

.. figure:: images/setup-12.png

.. figure:: images/setup-13.png

Verify installation
~~~~~~~~~~~~~~~~~~~

Open the windows application "Git Bash":

.. figure:: images/start-menu.png
    :alt: Start Menu

.. figure:: images/start-bash.png
    :alt: Git Bash

Verify Git version with

.. code-block:: bash

    $ git version
