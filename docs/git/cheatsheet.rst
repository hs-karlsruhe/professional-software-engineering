Git Cheatsheet
==============

.. contents:: Table of contents
    :local:

Glossary
--------

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Term
      - Description
    * - commit
      - Captures a snapshot of the project's currently staged changes.
    * - branch
      - A pointer to a specific commit.
    * - tag
      - A pointer to a specific commit. Additional, an annotated tag stores some meta information.
    * - fork
      - A server-side copy of another server-side repository
    * - remote
      - A server-side repository
    * - origin
      - The name of the default remote of your repository
    * - repository
      - A Git project

Git commands you can't live without
-----------------------------------

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Command
      - Description

    * - ``git init``
      - Creates empty Git repo

    * - ``git clone <remote>``
      - Clones Git repo located at ``<remote>``

    * - ``git add <directory/file>``
      - Stage all changes in ``<directory>`` or ``<file>`` for the next commit

    * - ``git add --all``
      - Stage all changes in the working directory

    * - ``git status``
      - List which files are staged, unstaged and untracked

    * - ``git commit -m "<message>"``
      - Commit the staged snapshot with ``<message>`` as commit message

    * - ``git commit -am "<message>"``
      - Commit all files in the working and staging area with ``<message>`` as commit message

    * - ``git log``
      - Display the entire commit history using the default format. For customizing see additional options.

    * - ``git diff``
      - Show unstaged changes between your index (HEAD) and working directory

    * - ``git diff --staged``
      - Show difference between staged changes and last commit

    * - ``git diff HEAD``
      - Show difference between working directory (modified and staged changes) and last commit

    * - ``git rm``
      - Remove files from the index

    * - ``git restore``
      - Undo modifications of files (be careful because of unintended data loss)

    * - ``git restore --staged``
      - Remove files from staging area

    * - ``git branch``
      - List of all branches in repo

    * - ``git checkout``
      - Create new branch

    * - ``git checkout -b``
      - Create and checkout new branch

    * - ``git rebase <base>``
      - Rebase the current branch onto ``<base>``

    * - ``git merge <branch>``
      - Merge ``<branch>`` into current ``<branch>``

    * - ``git remote add <name> <url>``
      - Create a new connection to a remote repo

    * - ``git fetch <remote> <branch>``
      - Fetches a specific ``<branch>`` from ``<remote>``. Leave off ``<branch>`` to fetch all remote refs.

    * - ``git pull <remote>``
      - Using defaults, executes two commands: ``git fetch <remote> <branch>`` and ``git merge <branch>``

    * - ``git pull --rebase <remote>``
      - Executes two commands: ``git fetch <remote> <branch>`` and ``git rebase <branch>``

    * - ``git push <remote> <branch>``
      - Push the ``<branch>`` to remote, along with necessary commits and objects. Creates named branch in the remote repo if it does't exists. ``<branch>`` defaults to current branch.

    * - ``git push <remote> --force``
      - Forces the git push even if it results in a non-fast-forward merge. Do not use the ``--force`` flag unless you’re absolutely sure you know what you’re doing.

    * - ``git push --set-upstream <remote> <branch>``
      - Pushes a ``<branch>`` that is not yet created at ``<remote>``.

    * - ``git tag <name>``
      - Creates a lightweight tag with name ``<name>``

    * - ``git tag -a <name> -m <message>``
      - Creates an annotated tag with ``<name>`` and commit message ``<message>``

.. note::

   ``<remote>`` defaults to ``origin`` if not specified

Git commands that simplify your life
------------------------------------

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Command
      - Description

    * - ``git config --global pull.rebase true``
      - Changes default behaviour from ``git pull`` from ``merge`` to ``rebase``

    * - ``git push <remote> --all``
      - Push all of your local branches to the specified remote.

    * - ``git push <remote> --tags``
      - Tags aren’t automatically pushed when you push a branch or use the ``--all`` flag. The ``--tags`` flag sends all of your local tags to the remote repo
