Terminal
========

.. contents:: Table of contents
    :local:

Git pager
---------

The Git pager view is entered when some git commands are executed, for example ``git log`` or ``git branch``.

.. figure:: images/git-pager.png
    :alt: Git pager
    :width: 800

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Keys
      - Action

    * - ``q``
      - Quit view

    * - ``<Down>``
      - Scroll down

    * - ``<Up>``
      - Scroll up

nano editor
-----------

.. figure:: images/nano.png
    :alt: nano
    :width: 800

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Keys
      - Action

    * - ``<Ctrl> + o``
      - Save file

    * - ``<Ctrl> + x``
      - Exit (when content was not changed)

    * - ``<Ctrl> + x, y, <Enter>``
      - Exit with saving file (when content was changed)

    * - ``<Ctrl> + x, n``
      - Exit without saving file (when content was changed)

vi editor
---------

.. figure:: images/vi.png
    :alt: vi
    :width: 800

The editor vi has 2 modes, the read mode and the insert mode. Only when the insert mode is entered (shown with ``-- INSERT --`` in the bottom left corner), text can be adjusted.

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Keys
      - Action
      - Usable in mode

    * - ``i``
      - Enter insert mode when in read mode
      - Read mode

    * - ``<Esc>``
      - Exit insert mode
      - Insert mode

    * - ``:w``
      - Save file
      - Read mode

    * - ``:q``
      - Quit (only works when content was not changed or file was already saved with ``:w``)
      - Read mode

    * - ``:q!``
      - Discard changes and quit without saving file (when content was changed or not changed)
      - Read mode

    * - ``:wq``
      - Exit with saving file (when content was changed)
      - Read mode
