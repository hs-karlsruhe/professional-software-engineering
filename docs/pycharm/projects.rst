Projects
========

.. contents:: Table of contents
    :local:

Create a new project
--------------------

A new project can be created by clicking "New Project" in the welcome window or in the main window under ``File`` -> ``New Project``.

.. figure:: images/pycharm-welcome-window.png
    :alt: welcome

After choosing "Pure Python", a location must be chosen. The new environment must be set to "Pipenv". If another Python version than the selected one should be used, it can be changed. By clicking "Create", the project will be created and opened in the main window.

.. figure:: images/pycharm-create-project.png
    :alt: create project

A new package can be created by right clicking the project name in the tree view, then "New" and "Python Package".

.. figure:: images/pycharm-create-package.png
    :alt: create package

A new module can be created by right clicking a package folder in the tree view, then "New" and "Python File".

.. figure:: images/pycharm-create-module.png
    :alt: create module

Open an existing project
------------------------

This manual requires an existing Python project which is initialized with pipenv.

.. figure:: images/welcome-to-pycharm.jpg
    :alt: welcome
    :width: 800

Click ``Open`` at the welcome page to select an existing project to open. Later on, if a project is already opened, you can also click on ``File -> Open...``.

Running a Python script
-----------------------

PyCharm recognizes the code line ``if __name__ == "__main__":`` and shows a play button to run the python script.

.. figure:: images/run-main.png
    :alt: welcome

Installation of a dependency
----------------------------

There are two possibilities of installing Python dependencies with Pipenv.

- Python packages can be installed by running ``pipenv`` command in the PyCharm (or any other) terminal.

   .. figure:: images/pycharm-terminal-pipenv-install.png
       :alt: terminal pipenv install

- When the Pipfile is changed in the editor, PyCharm recognizes the changes and offers a link to run ``pipenv update``. The command ``pipenv update`` can also be executed manually in the terminal.

   .. figure:: images/pycharm-pipfile.png
       :alt: pipfile

   .. figure:: images/pycharm-pipenv-update.png
       :alt: pipenv update

