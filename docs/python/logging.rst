Logging
=======

Using ``print`` for logging is bad practice (except some use cases like command line tools) because

-  ``print()`` does not have a context (no information where the output is generated, no information about the importance)
-  output resulting from ``print()`` is always printed, there is no switch to stop it
-  if ``print()`` is used frequently, the output gets very messy
-  dependencies could be unusable

The use of the logging framework is highly recommended because of its advantages.

-  Loggers record events during program execution
-  Events contains a level, a message and additional meta information (which are automatically set by the logging framework)
-  Logging is very helpful for debugging
-  Every Python module can (and should) use a separate logger instance so they are distinguishable
-  Loggers are highly customizable
   -  Formatting the output (file name, module, function, line of code, level, message, timestamp, process, thread and so on)
   -  Filter for filtering only desired log events (e.g. by logger names)
   -  Type of output (terminal, file, external services etc.)

Python delivers a logging framework in its standard library. While this section contains only the most important information about logging in Python, you can get more detailed information at https://docs.python.org/3/howto/logging.html.

Here is an example that uses the root logger as well as a separate logger for the current module.

.. code:: python

    import logging

    # See https://docs.python.org/3/howto/logging.html#changing-the-format-of-displayed-messages
    # how to customize log message format
    logging.basicConfig(level=logging.INFO, format=logging.BASIC_FORMAT)

    # Set the logger name to the name of the module
    logger = logging.getLogger(__name__)


    # See https://docs.python.org/3/tutorial/modules.html#executing-modules-as-scripts
    # why this is used
    if __name__ == "__main__":

        # Log to root logger
        logging.info("First message")

        # Log to logger instance
        logger.debug("My debug message")
        logger.info("Hello World")
        logger.warning("Terminating")

When executing the file, the output is:

.. code-block::

    INFO:root:First message
    INFO:__main__:Hello World
    WARNING:__main__:Terminating

The message ``My debug message`` is not being printed because the logger was configured to only print messages up to level ``INFO``. When you want to see the debug messages, you have to change the level to ``DEBUG``.
