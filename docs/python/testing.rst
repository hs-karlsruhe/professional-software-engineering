Testing
=======

Running tests in Python is as easy as executing

.. code-block:: bash

    $ pytest

on the command line. Or with additional code coverage output, run

.. code-block:: bash

    $ pytest --cov=<package-name>

Everything about testing can be found at https://docs.pytest.org/en/latest/index.html.

Tests files are placed in the ``tests/`` folder. Only files prefixed with ``test_`` are recognized py Pytest for test evaluations. You should use the original file name you want to test after the prefix. So when you named a file ``math.py``, the test file should be ``test_math.py``.

If you want to use functions of your package and modules, you must initiate your ``tests/`` folder also as a package by adding an empty
``__init__.py`` file.

Like test files, test functions must also start with prefix ``test_`` so that Pytest recognizes it as test function.

``assert`` is a keyword for evaluating a condition. If any assertion in the test function is false, the test will fail.

.. code:: python

    def sum(operand1, operand2):
        return operand1 + operand2


    def test_sum():
        assert sum(1, 3) == 4
