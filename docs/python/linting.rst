Linting
=======

    Pylint is a tool that checks for errors in Python code, tries to enforce a coding standard and looks for code smells. It can also
    look for certain type errors, it can recommend suggestions about how particular blocks can be refactored and can offer you details about the code's complexity.

    Pylint will display a number of messages as it analyzes the code and it can also be used for displaying some statistics about the number of warnings and errors found in different files. The messages are classified under various categories such as errors and warnings.

    Last but not least, the code is given an overall mark, based on the number and severity of the warnings and errors.

    -- \ http://pylint.pycqa.org/en/latest/intro.html\

The Pylint rules can be configured by providing a config file. Because the default rules are a bit too restrictive, you should apply the ruleset from https://gitlab.com/hs-karlsruhe/ci-templates/-/blob/master/pylintrc. Download this file and place it in the root of your project. Pylint will automatically use this file as ruleset (more detailed information see at http://pylint.pycqa.org/en/latest/user_guide/run.html#command-line-options ).

Run

.. code:: bash

    $ pylint <package>

to lint all your files in your project folder. Replace ``<package>`` with the name of the desired package.

Here is an example output:

.. code::

    $ pylint my_fourth_project
    ************* Module my_fourth_project.helloworld
    my_fourth_project/helloworld.py:1:0: C0114: Missing module docstring (missing-module-docstring)
    my_fourth_project/helloworld.py:2:0: C0116: Missing function or method docstring (missing-function-docstring)

    ------------------------------------------------------------------
    Your code has been rated at 5.00/10

Every module is listed in a section. Linting results are printed with the name of the file and the position in that file (line and character) followed by the linting code identifier and a human readable description. At the end, the code rating is printed.
