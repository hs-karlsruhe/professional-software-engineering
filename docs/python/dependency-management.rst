Dependency management
=====================

.. contents:: Table of contents
    :local:

Pip
---

Pip is the package manager for installing Python dependencies. It is installed with Python by default.

To install a new package run

.. code-block:: bash

    pip install <package>

and to update an existing package to the latest version run

.. code-block:: bash

    pip install --upgrade <package>

Virtual environment
-------------------

Native Python installations only support one version of a package at a time. If a package is added to the native Python installation, all Python programs can use it. At first this might sound nice because it saves efforts but for developers it is a real nightmare. Why? Let's see.

A developer

-  wants to use different versions of packages for different tools
-  wants to install and test packages without breaking functionalities of other tools
-  wants to easily setup a developer environment for new projects
-  needs a constant development environment for collaboration (not "works on my machine" like)

To adress all of the points above, Python introduced sandboxes which are known as *virtual environments*. Especially on Linux the usage of virtual environments is essential because many operating system tools are implemented in Python (e.g. package manager apt). When installing or deleting Python dependencies it can result in a dysfunctional operating system.

Technically, a virtual environment is a clone of a native Python installation which is copied to a custom folder (typically in folder ``.venv`` or ``venv`` in a project's root).

Pipenv
------

Pip does not natively support virtual environments. So there is a need of another tool that combines the power of pip and virtual envs. Pipenv is a feature rich tool that is developed for exactly that use case.

Typically, a distinction is made between two types of dependencies. On the one side there are dependencies needed only for development purposes (especially for testing or compiling) and on the other side there are dependencies needed at runtime. When delivering a software, only the minimum required set of dependencies should be shipped to minimize the resource consumption and complexity.

To honor this development pattern, pipenv differentiates between the development packages (``dev-packages``) and runtime packages (``packages``).

Setup pipenv (only once)
~~~~~~~~~~~~~~~~~~~~~~~~

Before you can work with pipenv, you have to do some setup steps for your operating system.

1. Install pipenv

   .. code-block:: bash

       $ pip install pipenv

   When using Linux, you probably must run this command with ``sudo``.

2. Check pipenv installation

   .. code-block:: bash

       $ pipenv --version

3. Set environment variable ``PIPENV_VENV_IN_PROJECT=1``

   As mentioned above, the virtual environment typically is stored in a ``.venv`` or ``venv`` folder. By default, pipenv does not create that folder in the current project but uses another path outside the projects for all pipenv instances. But it is a lot easier to work on projects where the ``.venv`` folder is placed inside the project. We can enforce pipenv to create the ``.venv`` folder inside the project by setting the environment variable.

   **Windows**

    #. Open the System Settings dialog by searching for "environment variables" (German: "Umgebungsvariable") and open the window "Edit the system variables".

        .. figure:: images/pipenv-open-system-settings.png
            :width: 600

    #. Then click the button "Environment Variables" (German: "Umgebungsvariablen").

        .. figure:: images/pipenv-open-environment-settings.png
            :width: 450

    #. Click the button "New" (German: "Neu") to create a new environment variable.

        .. figure:: images/pipenv-add-new-environment-variable.png
            :width: 600

    #. Set the name to ``PIPENV_VENV_IN_PROJECT`` and the value to ``1``.

        .. figure:: images/pipenv-create-pipenv-environment-variable.png
            :width: 600

    #. Now close all the opened dialogs from above.

    #. To verify that the environment variable is set, run a new Git Bash and execute ``echo $PIPENV_VENV_IN_PROJECT``. You should now see the returned value ``1``.

    .. note::
        Terminals and PyCharm has to be terminated because the variable is only available after a new start after the change of the variable.

   **Linux**

   .. code-block:: bash

       $ export PIPENV_VENV_IN_PROJECT=1

   This variable is only valid for the current shell context. To permanently add the environment variable, add the line

   .. code-block:: bash

       PIPENV_VENV_IN_PROJECT=1

   to the files ``$HOME/.pam_environment`` and ``$HOME/.profile``.

Initialize a new project with pipenv (for every new project)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before you can start developing a python project, you have to initialize the project with pipenv. Once your project is ready, you can install the required dependencies.

To initialize a new project, execute the following tasks:

1. Create a new folder for the project (or use an already existing one)

   .. code-block:: bash

       $ mkdir -p $HOME/gitlab.com/hs-karlsruhe/my-first-pipenv-project

2. Navigate to the folder

   .. code-block:: bash

       $ cd $HOME/gitlab.com/hs-karlsruhe/my-first-pipenv-project

3. Initialize pipenv project in the existing folder

   This step will create a new virtual environment in folder ``.venv`` in the project.

   .. code-block::

        $ pipenv --python 3
        Creating a virtualenv for this project…
        Pipfile: /home/sm/git/gitlab.com/hs-karlsruhe/my-first-pipenv-project/Pipfile
        Using /usr/bin/python3.8 (3.8.2) to create virtualenv…
        ⠏ Creating virtual environment...created virtual environment CPython3.8.2.final.0-64 in 635ms
        creator CPython3Posix(dest=/home/sm/git/gitlab.com/hs-karlsruhe/my-first-pipenv-project/.venv, clear=False, global=False)
        seeder FromAppData(download=False, CacheControl=latest, pyparsing=latest, requests=latest, pytoml=latest, pkg_resources=latest, idna=latest, lockfile=latest, certifi=latest, pip=latest, distlib=latest, retrying=latest, urllib3=latest, pep517=latest, html5lib=latest, msgpack=latest, wheel=latest, appdirs=latest, progress=latest, packaging=latest, distro=latest, six=latest, setuptools=latest, contextlib2=latest, webencodings=latest, chardet=latest, colorama=latest, ipaddr=latest, via=copy, app_data_dir=/home/sm/.local/share/virtualenv/seed-app-data/v1.0.1.debian)
        activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

        ✔ Successfully created virtual environment!
        Virtualenv location: /home/sm/git/gitlab.com/hs-karlsruhe/my-first-pipenv-project/.venv
        Creating a Pipfile for this project…

   When you check the files and folders of your project, you will see the folder ``.venv`` and the file ``Pipfile``.

   .. code-block:: bash

       $ ls -a
       .  ..  Pipfile  .venv

   The content of ``Pipfile`` will look like this:

   .. code-block::

       $ cat Pipfile
       [[source]]
       name = "pypi"
       url = "https://pypi.org/simple"
       verify_ssl = true

       [dev-packages]

       [packages]

       [requires]
       python_version = "3.8"

   So you can see some information about the source of pip packages, a (empty) list of ``dev-packages`` and ``packages`` and also the python version you choose.

4. Install pip development packages ``pytest``, ``pytest-cov`` and ``pylint``

   These packages are strongly recommended for every python project to prepare it for testing and linting.

   .. code-block::

       $ pipenv install --dev pytest pytest-cov pylint
       Installing pytest…
       Adding pytest to Pipfile's [dev-packages]…
       ✔ Installation Succeeded
       Installing pytest-cov…
       Adding pytest-cov to Pipfile's [dev-packages]…
       ✔ Installation Succeeded
       Installing pylint…
       Adding pylint to Pipfile's [dev-packages]…
       ✔ Installation Succeeded
       Pipfile.lock not found, creating…
       Locking [dev-packages] dependencies…
       ✔ Success!
       Locking [packages] dependencies…
       Updated Pipfile.lock (86692b)!
       Installing dependencies from Pipfile.lock (86692b)…
        🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 18/18 — 00:00:07
       To activate this project's virtualenv, run pipenv shell.
       Alternatively, run a command inside the virtualenv with pipenv run.

   After the first installation of a new package, pipenv creates the new file ``Pipfile.lock``.

   .. code-block:: bash

       $ ls -a
       .  ..  Pipfile  Pipfile.lock  .venv

   In this file, all recurse dependencies are listed and saved with there installed package version.

Initialize an existing project with pipenv (for every existing project)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When working with an existing pipenv project, all the initialization steps above are already executed. To work with an existing project is as simple as going into the project directory and execute pipenv install command.

.. code-block:: bash

    $ cd $HOME/gitlab.com/hs-karlsruhe/existing-pipenv-project
    $ pipenv install --dev

With this command all ``dev-packages`` and ``packages`` are installed based on the entries in ``Pipfile`` and ``Pipfile.lock``.

Installing runtime dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To install runtime dependencies, you must run ``pipenv install`` without option ``--dev``.

.. code-block:: bash

    $ pipenv install <name-of-package>

Running commands inside virtual environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default, the Python installation of your operating system is used when running ``python`` command.

There are two options to run commands from inside the virtual environment. To activate this project's virtualenv permanently for the current shell, run ``pipenv shell``. Alternatively, run a command once inside the virtualenv with ``pipenv run``.
