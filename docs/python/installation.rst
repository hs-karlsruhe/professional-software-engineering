Installation
============

Download the latest Python version from https://www.python.org/downloads/ and install it.

.. note::
    The following instructions are for Python version 3.9 but it should still be valid for future versions.

Windows
-------

#. Activate checkbox "Add Python 3.9 to PATH" so that Python is accessible over the command line. Choose customized installation.

   .. figure:: images/setup-1.png

#. Activate all checkboxes:

   .. figure:: images/setup-2.png

#. Change the install location if desired and click "Install".

   .. figure:: images/setup-3.png

#. You can check for a successful installation by running the command

   .. code:: bash

        python --version

   in a terminal (e.g. Git Bash). The version of Python should be printed.

Mac
---

#. Install Python according to the screenshots:

    .. figure:: images/setup-mac-1.png
        :width: 700

    .. figure:: images/setup-mac-2.png
        :width: 700

    .. figure:: images/setup-mac-3.png
        :width: 700

    .. figure:: images/setup-mac-4.png
        :width: 700

    .. figure:: images/setup-mac-5.png
        :width: 700

#. You can check for a successful installation by running the command

   .. code:: bash

        python --version

   in the terminal. The version of Python should be printed.
