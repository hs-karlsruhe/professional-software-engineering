# Lecture Methods of Professional Software Engineering

[![pipeline status](https://gitlab.com/hs-karlsruhe/professional-software-engineering/badges/master/pipeline.svg)](https://gitlab.com/hs-karlsruhe/professional-software-engineering/commits/master)
[![documentation](https://hs-karlsruhe.gitlab.io/professional-software-engineering/docs/documentation.svg)](https://hs-karlsruhe.gitlab.io/professional-software-engineering/docs/)
